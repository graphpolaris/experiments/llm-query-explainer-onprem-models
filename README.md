# LLM Query Explainer OnPrem Models

## Name
Knowledge Query Graph Explainer

## Description
This program is designed to be used with local LLMs. It contains a prompt and the option to add information from a graph schema and a list of query results to the prompt. This prompt will then be sent to the LLM to generate five responses. Three models are included, but more models can easily be added by the user. The prompt is based on a knowledge graph and a list of query results, which should be supplied by the user. Some mock data is also included for testing purposes.

## Usage
To use the program simply run main.py with the preferred graph schema and query results as such:
main.py {schema} {query}
If these parameters are not specified, the default mock data will be used.



## Authors and acknowledgment
Developed by Yme de Jong
